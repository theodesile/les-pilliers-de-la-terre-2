package view;

import controller.GameController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

/*
Classe mère faite pour être implémentée et représenter une fenetre de dialogue, on l'ouvre en faisant new FenetreDialogue()
 */
public class FenetreDialogue extends Stage {
    public Pane root;
    FenetreDialogue(){
        super();
    }
    public FenetreDialogue(String title, String content, String peoples, boolean waitForVisibility) {
        super();
        try {
            root = FXMLLoader.load(getClass().getResource("fxml/fenetreDialogue.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        root.getStylesheets().add(getClass().getResource("css/fenetreDialogue.css").toExternalForm());
        setScene(new Scene(root, 600, 400));
        setResizable(false);
        Text t = (Text) root.lookup("#title");
        t.setFill(Color.WHITE);
        t = (Text) root.lookup("#content");
        t.setFill(Color.WHITE);
        t = (Text) root.lookup("#peoples");
        t.setFill(Color.WHITE);
        setTitle(title);
        setFenTitle(title);
        setContent(content);
        setPeoples(peoples);
        setAlwaysOnTop(true);
        if(waitForVisibility) {
            GameController.lFenetre.add(this);
            setOnCloseRequest(windowEvent -> GameController.updateFen());
            if (GameController.lFenetre.size() == 1)
                show();
        }
        else
            show();
    }
    @Override
    public void close(){
        GameController.updateFen();
        super.close();
    }
    //permet de changer le titre
    public void setFenTitle(String title){
        Text t = (Text) root.lookup("#title");
        t.setText(title);
    }
    //permet de changer le contenu
    public void setContent(String content){
        Text t = (Text) root.lookup("#content");
        t.setText(content);
    }
    //permet de changer les gens à qui est adressée la fenetre de dialogue
    public void setPeoples(String peoples){
        Text t = (Text) root.lookup("#peoples");
        t.setText(peoples);
    }
    /*méthode qui permettra de charger le formulaire de la fenetre de dialogue (boutons pour faire un choix, formulaire d'entrée de texte, etc...
    */
    public void loadForm(){};
}