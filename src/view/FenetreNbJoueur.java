package view;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import view.FenetreDialogue;

import java.io.IOException;

public class FenetreNbJoueur extends FenetreDialogue {
    private TextField textField;
    private Button bhaut;
    private Button bbas;
    private Button valider;

    public FenetreNbJoueur() throws IOException {
        super("Nouvelle partie","Choisissez le nombre de joueurs : ","", false);

        setFenTitle("");
        loadForm();
    }

    @Override
    public void loadForm() {
        VBox mainbox = new VBox();
        HBox box = new HBox();
        textField = new TextField();
        textField.setMaxWidth(40);
        textField.setText("2");
        VBox vbox = new VBox();
        bhaut = new Button("V");
        bhaut.setRotate(180);
        bbas = new Button("V");
        vbox.getChildren().add(bhaut);
        vbox.getChildren().add(bbas);
        box.getChildren().add(textField);
        box.getChildren().add(vbox);
        bhaut.setMaxHeight(textField.getHeight()/2);
        vbox.setMaxHeight(textField.getHeight());
        bbas.setMaxHeight(textField.getHeight()/2);
        mainbox.getChildren().add(box);
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        mainbox.setLayoutX(250);
        anchorPane.getChildren().add(mainbox);
        valider = new Button("Valider");
        valider.setStyle("-fx-start-margin: 20px");
        Rectangle r = new Rectangle(30,30);
        r.setVisible(false);
        mainbox.getChildren().add(r);
        mainbox.getChildren().add(valider);
    }

    public Button getBbas() {
        return bbas;
    }

    public Button getBhaut() {
        return bhaut;
    }

    public TextField getTextField() {
        return textField;
    }

    public Button getValider() {
        return valider;
    }
}
