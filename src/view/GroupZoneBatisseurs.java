package view;


import javafx.scene.Group;
import model.ZonesBatisseurs;

import java.util.ArrayList;

public class GroupZoneBatisseurs extends Group {
    private ArrayList<ZoneBatisseurView> lZonesBatisseurs;
    private int numGroup;
    public GroupZoneBatisseurs(int numGroup){
        super();
        lZonesBatisseurs = new ArrayList<>();
        this.numGroup=numGroup;
    }
    public ZoneBatisseurView addZoneBatisseur(int x, int y){
        ZoneBatisseurView zb=new ZoneBatisseurView(x,y, numGroup);
        lZonesBatisseurs.add(zb);
        getChildren().add(zb);
        return zb;
    }

    public ArrayList<ZoneBatisseurView> getlZonesBatisseurs() {
        return lZonesBatisseurs;
    }

    public int getNumGroup() {
        return numGroup;
    }
}
