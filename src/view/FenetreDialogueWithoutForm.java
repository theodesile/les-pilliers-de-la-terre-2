package view;

import view.FenetreDialogue;

import java.io.IOException;

public class FenetreDialogueWithoutForm extends FenetreDialogue {
    public FenetreDialogueWithoutForm(String title, String content, String peoples)  {
        setTitle(title);
        setFenTitle(title);
        setContent(content);
        setPeoples(peoples);
    }

    @Override
    public void loadForm() {}
}
