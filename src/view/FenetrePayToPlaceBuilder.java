package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.print.PageLayout;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import model.Player;
import model.PlayersColors;
import model.ressources.Ressource;

import javafx.scene.control.Button;


public class FenetrePayToPlaceBuilder extends FenetreDialogue{
    private boolean wantToPay;
    private int somme;
    private Player player;
    private Button pay;
    private Button notPay;
    public FenetrePayToPlaceBuilder(int somme, Player player){
        super("","","",true);
        this.somme=somme;
        this.player=player;
        setPeoples("Player " + player.getColor().getName());
        setTitle("Placer un bâtisseur");
        String content;
        if(somme>0){
            if(player.getInventory().get(Ressource.GOLD)>=somme){
                content = "Pour placer un batisseur vous devez payer " + somme + " or le faites-vous ?";
            }
            else{
                content = "Pour placer un batisseur vous devez payer " + somme + " or mais vous n'avez pas assez d'or, vous pouvez seulement le mettre de côté";
            }
        }
        else{
            content = "A vous de placer votre bâtisseurs.";
        }
        setContent(content);
        loadForm();
    }

    @Override
    public void loadForm(){
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        if(somme>0){
            if(player.getInventory().get(Ressource.GOLD)>=somme){
                pay = new Button("Payer");
                notPay = new Button("Ne pas Payer");
                hBox.getChildren().add(pay);
            }
            else{
                notPay = new Button("Passer");
            }
            hBox.getChildren().add(notPay);
        }
        else{
            pay = new Button("Valider");
            hBox.getChildren().add(pay);
        }
        hBox.setLayoutX(anchorPane.getWidth()/2-hBox.getWidth());
        anchorPane.getChildren().add(hBox);
    }

    public Button getNotPay() {
        return notPay;
    }

    public Button getPay() {
        return pay;
    }
}
