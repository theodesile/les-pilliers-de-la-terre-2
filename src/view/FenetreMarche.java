package view;

import controller.GameController;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.Market;
import model.Player;
import model.ressources.Ressource;

public class FenetreMarche extends FenetreDialogue {
    private GameController gameController;

    public FenetreMarche(GameController game) {
        super("Marché de KingsBridge", "", "tous les joueurs", true);
        String content="Bienvenue sur au marché de kingsbridge, ici vous pouvez vendre ou acheter des ressources au marche. \n" +
                "Cependant seuls les joueurs ayant placé un de leur bâtisseurs sur le marché pourront en bénéficier.";
        setContent(content);
        this.gameController =game;
        Button start = new Button("Commencer");
        start.setOnAction(actionEvent -> gameController.startMarket());
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        anchorPane.getChildren().add(start);
        start.setLayoutX(220);
        start.setLayoutY(50);
    }
    public void play(Player p, Market m){
        setContent("");
        setPeoples("Joueur " + p.getColor());
        GridPane gridPane = new GridPane();
        gridPane.setHgap(20);
        gridPane.setVgap(30);
        int column=0;
        for(Ressource r: Ressource.values()){
            if(r.isExcheangeable()) {
                Text t = new Text(r.getName() + " ( " + r.getValue() + " or " + ")");
                t.setFill(Color.WHITE);
                t.setStyle("-fx-font-size: 20px; -fx-font-family: 'Corbel Light'");
                gridPane.add(t,column,0);
                ChoiceBox cb = new ChoiceBox();
                cb.setItems(FXCollections.observableArrayList());
                cb.setValue("Quantité");
                cb.getItems().add("Quantité");
                Button sellButton = new Button("Vendre");
                sellButton.setOnAction(actionEvent -> sell(r,p, cb));
                Button buyButton = new Button("Acheter");
                buyButton.setOnAction(actionEvent -> buy(r,p, cb));
                for(int i=0; i<m.getAvailableRessources().get(r); i++){
                    cb.getItems().add(Integer.toString(i+1));
                }
                gridPane.add(cb,column,1 );
                gridPane.add(sellButton, column, 2);
                gridPane.add(buyButton, column, 3);
                column+=1;
            }
        }
        gridPane.setLayoutX(110);
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        anchorPane.getChildren().add(gridPane);
        Button buttonPasser = new Button("Passer");
        buttonPasser.setOnAction(actionEvent -> gameController.passMarketTurn(p));
        anchorPane.getChildren().add(buttonPasser);
        buttonPasser.setLayoutX(50);
        buttonPasser.setLayoutY(240);
    }
    private void sell(Ressource r, Player p, ChoiceBox cb){
        gameController.sellToMarket(r,p,cb);
    }
    private void buy(Ressource r, Player p, ChoiceBox cb){
        gameController.buyToMarket(r,p,cb);
    }
}
