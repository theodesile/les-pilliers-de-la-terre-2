package view;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import model.PlayersColors;
import view.FenetreDialogue;

import java.io.IOException;
import java.util.ArrayList;

public class FenetreColorJoueur extends FenetreDialogue {

    private final ArrayList<PlayersColors> availableColors;
    private EventHandler controller;
    private Button valider;
    private ChoiceBox cb;

    public FenetreColorJoueur(int n, ArrayList<PlayersColors> avalaibleColors, EventHandler controller) throws IOException {
        super("","Veuillez choisir votre couleur Joueur " + n,"Joueur " + n, false);
        this.availableColors=avalaibleColors;
        this.controller=controller;
        setFenTitle("");
        loadForm();
    }

    @Override
    public void loadForm() {
        VBox vbox = new VBox();
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        cb = new ChoiceBox();
        cb.setItems(FXCollections.observableArrayList());
        for(PlayersColors pc: availableColors){
            cb.getItems().add(pc.getName());
        }
        vbox.getChildren().add(cb);
        anchorPane.getChildren().add(vbox);
        Rectangle r = new Rectangle(50,50);
        r.setVisible(false);
        vbox.getChildren().add(r);
        valider = new Button("Valider");
        valider.setOnAction(controller);
        vbox.getChildren().add(valider);
        vbox.setLayoutX(250);
        cb.setValue(0);
    }

    public Button getValider() {
        return valider;
    }

    public ChoiceBox getCb() {
        return cb;
    }
}
