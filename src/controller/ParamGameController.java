package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import model.Player;
import model.PlayersColors;
import view.FenetreColorJoueur;
import view.FenetreNbJoueur;

import java.io.IOException;
import java.util.ArrayList;

/*
Permet de faire les choix du nombre de joueurs et de la couleur de chaque joueur
 */
public class ParamGameController implements EventHandler {
    private FenetreNbJoueur fenetreNbJoueur;
    private int nbJoueurs;
    private FenetreColorJoueur currentFenColor;
    private ArrayList<Player> lplayers;
    private ArrayList<PlayersColors> availableColor;

    public ParamGameController() throws IOException {
        this.fenetreNbJoueur = new FenetreNbJoueur();
        nbJoueurs=-1;
        fenetreNbJoueur.getValider().setOnAction(this);
        fenetreNbJoueur.getBhaut().setOnAction(this);
        fenetreNbJoueur.getBbas().setOnAction(this);
        lplayers=new ArrayList<>();
    }

    @Override
    public void handle(Event event) {
        if (fenetreNbJoueur.getBbas()==event.getTarget()) {
            incremente(-1);
        }
        else if(fenetreNbJoueur.getBhaut()==event.getTarget()){
            incremente(1);
        }
        else if(fenetreNbJoueur.getValider()==event.getTarget()){
            try {
                nbJoueurs = Integer.parseInt(fenetreNbJoueur.getTextField().getText());
                if(nbJoueurs<2 || nbJoueurs>4)
                    return;
            }catch (NumberFormatException e){return;}
            fenetreNbJoueur.close();
            try {
                makeColorsChoices();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(currentFenColor!=null && currentFenColor.getValider()==event.getTarget()) {
            PlayersColors pc;
            if (!(currentFenColor.getCb().getValue() instanceof String)) {
                return;
            }
            switch ((String) currentFenColor.getCb().getValue()) {
                case "bleu":
                    pc = PlayersColors.BLUE;
                    break;
                case "rouge":
                    pc = PlayersColors.RED;
                    break;
                case "vert":
                    pc = PlayersColors.GREEN;
                    break;
                default:
                    pc = PlayersColors.YELLOW;
                    break;
            }
            availableColor.remove(pc);
            lplayers.add(new Player(pc));
            currentFenColor.close();
            try {
                makeColorsChoices();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (availableColor.size() == 4 - nbJoueurs){ try {
                new GameController(lplayers);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



        }



    }

    private void makeColorsChoices() throws IOException {
        if(availableColor==null) {
            availableColor = new ArrayList<>();
            for (PlayersColors color : PlayersColors.values()) {
                availableColor.add(color);
            }
        }
        if(lplayers.size()<nbJoueurs){
            currentFenColor = new FenetreColorJoueur(lplayers.size()+1, availableColor, this);
        }
    }

    private void incremente(int i) {
        int n= Integer.parseInt(fenetreNbJoueur.getTextField().getText());
        if(n+i<=4 && n+i>1) {
            n += i;
            fenetreNbJoueur.getTextField().setText(Integer.toString(n));
        }
    }
    public ArrayList<Player> getLplayers() {
        return lplayers;
    }
}
