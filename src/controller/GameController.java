package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.ChoiceBox;
import model.*;
import model.ressources.Ressource;
import view.*;

import java.lang.Math;
import java.io.IOException;
import java.util.ArrayList;

public class GameController implements EventHandler {
    private Game game;
    private FenetreJeu fenetreJeu;
    private ArrayList<Player> lplayers;
    private Market market;
    private FenetreMarche fenetreMarche;
    public static ArrayList<FenetreDialogue> lFenetre = new ArrayList<>();



    public GameController(ArrayList<Player> lplayers) throws IOException {
        game = new Game(lplayers);
        this.lplayers = lplayers;
        int g = 20;
        for (Player p : lplayers) {
            p.getInventory().put(Ressource.GOLD, g);
            g++;
        }
        this.fenetreJeu = new FenetreJeu(lplayers);
        fenetreJeu.getButtonNext().setOnAction(this);
        String content = "Bienvenue dans cette nouvelle partie des Pilliers de la terre vous avez 7 tours pour obtenir le maximum de points de victoire";
        new FenetreDialogue("Les Pilliers de la Terre", content, "Tous les joueurs", true);
        playNextTurn();
    }

    public static void updateFen() {
        if (lFenetre.size() > 0)
            lFenetre.remove(0);
        if (lFenetre.size() > 0)
            lFenetre.get(0).show();
    }

    @Override
    public void handle(Event event) {
        if (event.getSource() == fenetreJeu.getButtonNext())
            playNextTurn();
    }

    private void playNextTurn() {
        market = new Market();
        fenetreJeu.clearZoneBatisseurs();
        if (game.toNextPlayer())
            fenetreJeu.nextTurn();
        fenetreJeu.updateTextInfo();
        if (game.getNbToursRestants() == 0 && game.getIndexCurrentPlayer() == 0) {
            endGame();
            return;
        }
        String content1 = "Il reste " + Integer.toString(game.getNbToursRestants()) + " tours " + " chaque joueurs récupère les ressources sur " +
                "lesquelles il a placé ses pions pour chaque ouvrier posé";
        new FenetreDialogue("Les Pilliers de la Terre", content1, "Tous les joueurs", true);
        if (game.getCurrentPlayer().getPrivileges().get(Privilege.COUNT)) {
            new FenetreDialogue("privilège", "le compte Bartholomew vous a donné 2 pièces d'or",
                    "joureur " + game.getCurrentPlayer().getColor().getName(), true);
            game.getCurrentPlayer().getInventory().replace(Ressource.GOLD, game.getCurrentPlayer().getInventory().get(Ressource.GOLD) + 2);
        }
        new BuilderPlaceController(fenetreJeu, lplayers, this, game);
        if (game.getIndexCurrentPlayer() == 0 && game.getNbToursRestants() != 6 && game.getNbToursRestants() != 0) {
            //privilege count
            if (game.getCurrentPlayer().getPrivileges().get(Privilege.COUNT)) {
                new FenetreDialogue("privilège", "le compte Bartholomew vous a donné 2 pièces d'or",
                        "joureur " + game.getCurrentPlayer().getColor().getName(), true);
                game.getCurrentPlayer().getInventory().replace(Ressource.GOLD, game.getCurrentPlayer().getInventory().get(Ressource.GOLD) + 2);
            }
        }
    }


    private void endGame() {
        game.exchangeAllRessources();
        fenetreJeu.close();
        new AcceuilController();
        StringBuilder content = new StringBuilder("Bravo au joueur " + game.whoWon().getColor() + " qui a remporte la partie ! \n" +
                "\n voici le résumé des scores:\n");
        for (Player p : game.getlPlayers()) {
            content.append(p.getColor()).append(" a fait ").append(p.getNumberVictoryPoints()).append(" points \n");
        }
        new FenetreDialogue("Fin du jeu", content.toString(), "Tous les joueurs", true);
    }

    private void addPionTo(PawsZone pawsZone, Player p) {
        int initNumber = pawsZone.getNbPawsPerColor().get(p.getColor());
        pawsZone.getNbPawsPerColor().put(p.getColor(), initNumber + 1);
    }

    /*
    méthode pour faire jouer la taxe du roi
    @param LPlayerInKingCourt: liste des joueurs dont un des bâtisseur
    est dans la cour du roi
     */
    private void playKingsTax(ArrayList<Player> lPlayerInKingCourt) {
        int taxe = game.getKingTax();
        ArrayList<Player> taxedPlayer = new ArrayList<>();
        for (Player p : lplayers) {
            if (!lPlayerInKingCourt.contains(p))
                taxedPlayer.add(p);
        }
        new FenetreImpotRoi(taxe, taxedPlayer, lPlayerInKingCourt);
        game.applyKingTax(lplayers, lPlayerInKingCourt, taxe);
        fenetreJeu.updateTextInfo();
    }

    /*
    methode pour jouer le marché
    @param lplayerInMarket: liste triée des joueurs ayant leur batisseur dans le
    marché
     */
    public void playMarket(ArrayList<Player> lplayersInMarket) {
        market.initMarket();
        for (Player p : lplayersInMarket)
            market.addExchanger(p);
        fenetreMarche = new FenetreMarche(this);
    }

    public void sellToMarket(Ressource r, Player p, ChoiceBox cb) {
        try {
            int value = Integer.parseInt((String) cb.getValue());
            market.sell(r, value, p);
            fenetreJeu.updateTextInfo();
            fenetreMarche.play(market.getNextPlayer(), market);
        } catch (NumberFormatException n) {
            ;
        }
    }

    public void passMarketTurn(Player p) {
        market.removeExchanger(p);
        if (market.getlExchangers().size() == 0)
            fenetreMarche.close();
        else
            fenetreMarche.play(market.getNextPlayer(), market);
    }

    public void buyToMarket(Ressource r, Player p, ChoiceBox cb) {
        try {
            int value = Integer.parseInt((String) cb.getValue());
            market.buy(r, value, p);
            fenetreJeu.updateTextInfo();
            fenetreMarche.play(market.getNextPlayer(), market);
        } catch (NumberFormatException n) {
            ;
        }
    }
    public void startMarket() {
        fenetreMarche.play(market.getNextPlayer(), market);
    }
}