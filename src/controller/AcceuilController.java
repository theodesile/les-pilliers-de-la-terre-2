package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import view.FenetreAcceuil;

import java.io.IOException;

public class AcceuilController implements EventHandler {

    private FenetreAcceuil fen;

    public AcceuilController(){
        try {
            fen = new FenetreAcceuil();
            fen.getPlay().setOnAction(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void handle(Event event) {
        try {
            ParamGameController c =new ParamGameController();
            fen.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
