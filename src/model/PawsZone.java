package model;

import java.util.HashMap;
import java.util.Map;

public class PawsZone {
    private String name;
    private final Map<PlayersColors, Integer> nbPawsPerColor;

    public PawsZone(String name, int nbRedPlawns, int nbBluePawns, int nbYellowPawns, int nbGreenPawns) {
        this.name=name;
        nbPawsPerColor = new HashMap<>();
        nbPawsPerColor.put(PlayersColors.BLUE, 0);
        nbPawsPerColor.put(PlayersColors.RED, 0);
        nbPawsPerColor.put(PlayersColors.GREEN, 0);
        nbPawsPerColor.put(PlayersColors.YELLOW, 0);
    }

    public Map<PlayersColors, Integer> getNbPawsPerColor() {
        return nbPawsPerColor;
    }

    public String getName() {
        return name;
    }
}
