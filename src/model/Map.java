package model;

public class Map {
    private PawsZone zoneSable;
    private PawsZone zoneBois;
    private PawsZone courRoi;
    private PawsZone zonePierre;
    public Map(){
        zoneSable=new PawsZone("zone sable",0,0,0,0);
        zoneBois=new PawsZone("zone bois",0,0,0,0);
        zonePierre=new PawsZone("zone pierre",0,0,0,0);
        courRoi=new PawsZone("cour du roi",0,0,0,0);
    }

    public PawsZone getZoneSable() {
        return zoneSable;
    }

    public void setZoneSable(PawsZone zoneSable) {
        this.zoneSable = zoneSable;
    }

    public PawsZone getZoneBois() {
        return zoneBois;
    }

    public void setZoneBois(PawsZone zoneBois) {
        this.zoneBois = zoneBois;
    }

    public PawsZone getCourRoi() {
        return courRoi;
    }

    public void setCourRoi(PawsZone courRoi) {
        this.courRoi = courRoi;
    }

    public PawsZone getZonePierre() {
        return zonePierre;
    }

    public void setZonePierre(PawsZone zonePierre) {
        this.zonePierre = zonePierre;
    }
}
