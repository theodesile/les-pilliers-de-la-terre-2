package model;

public enum PlayersColors {

    RED("rouge"),
    BLUE("bleu"),
    YELLOW("jaune"),
    GREEN("vert");
    private String name;

    PlayersColors(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
