package model;

import exception.NegativeQuantityException;
import model.ressources.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Player {

    private PlayersColors color;
    private int numberVictoryPoints;
    private HashMap<Ressource,Integer> inventory;
    private HashMap<Privilege,Boolean> privileges;


    public Player(PlayersColors color) {

        this.color = color;
        this.numberVictoryPoints = 0;
        inventory = new HashMap<>();
        setupInventory();
        privileges = new HashMap<>();
        setupPrivileges();
    }


    private void setupInventory() {
        inventory.put(Ressource.BOIS,0);
        inventory.put(Ressource.METAL,0);
        inventory.put(Ressource.PIERRE,0);
        inventory.put(Ressource.SABLE,0);
        inventory.put(Ressource.MASTERBUILDER,3);
        inventory.put(Ressource.SMALLWORKER,12);
    }
    private void setupPrivileges() {
        for (Privilege p:Privilege.values()) {
            privileges.put(p,false);
        }
    }


    /**
     * Exchange resources and add the points to numberVictoryPoints
     */
    public void exchangeResources(){

        AtomicInteger points= new AtomicInteger();
        this.inventory.forEach((resource, quantity) -> {
            points.addAndGet(resource.exchange(quantity)); // exchange the resources
            quantity=0; // set the quantity of this resource to 0
        });
        addToNumberVictory(points.get());
    }

    public boolean pay(int value, boolean forcePay){
        int initGold=inventory.get(Ressource.GOLD);
        if(value<initGold){
            inventory.put(Ressource.GOLD, initGold-value);
            return true;
        }
        else if(forcePay){
            inventory.put(Ressource.GOLD, 0);
        }
        return false;
    }
    public HashMap<Ressource, Integer> getInventory() {
        return inventory;
    }

    public void addToInventory(Ressource r, Integer q) throws exception.NegativeQuantityException{

        if(q<0) throw new NegativeQuantityException();
        this.inventory.replace(r,inventory.get(r)+q);
    }

    public int getNumberVictoryPoints() {
        return numberVictoryPoints;
    }

    public PlayersColors getColor() {
        return color;
    }

    public void addToNumberVictory(int points){
        numberVictoryPoints+=points;
    }

    @Override
    public String toString() {

        return "Player " + color +
                "\nPoints:" + numberVictoryPoints +
                "\n inventory:" +
                "\n metal:" + inventory.get(Ressource.METAL) +
                " stone:" + inventory.get(Ressource.PIERRE) +
                "\n wood:" + inventory.get(Ressource.BOIS) +
                " sand:" + inventory.get(Ressource.SABLE) ;
    }

    public void addToVictoryPoints(int d) {
        this.numberVictoryPoints += d;
    }

    public void setNumberVictoryPoints(int numberVictoryPoints) {
        this.numberVictoryPoints = numberVictoryPoints;
    }

    public void setInventory(HashMap<Ressource, Integer> inventory) {
        this.inventory = inventory;
    }
    public HashMap<Privilege, Boolean> getPrivileges() {
        return privileges;
    }
}
