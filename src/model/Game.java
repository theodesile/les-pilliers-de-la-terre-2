package model;
import javafx.scene.control.ChoiceBox;
import model.ressources.Ressource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Game {
    private int indexCurrentPlayer;
    private ArrayList<Player> lPlayers;
    private Map map;
    private int nbToursRestants;
    private ArrayList<ZonesBatisseurs> lZonesBatisseurs;
    public Game(ArrayList players){
        lPlayers=players;
        map=new Map();
        nbToursRestants=7;
        initZonesBatisseurs();
    }

    private void initZonesBatisseurs(){
        lZonesBatisseurs = new ArrayList<>();
        lZonesBatisseurs.addAll(Arrays.asList(ZonesBatisseurs.values()));
    }
    public ArrayList<Player> getlPlayers() {
        return lPlayers;
    }

    private void refreshZonesBatisseurs(){
        for(ZonesBatisseurs zb: lZonesBatisseurs){
            zb.getlBatisseurs().clear();
        }
    }

    boolean addBatisseurToZone(PlayersColors batisseurColor, int numZone){
        if(numZone<1 || numZone>lZonesBatisseurs.size())
            return false;
        return lZonesBatisseurs.get(numZone-1).addBatisseur(batisseurColor);
    }

    public Player whoWon() {
        int max=lPlayers.get(0).getNumberVictoryPoints();
        Player winner=lPlayers.get(0);
        for(Player p: lPlayers ) {
            if(p.getNumberVictoryPoints()>max) {
                max=p.getNumberVictoryPoints();
                winner=p;
            }
        }
        return winner;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public int getNbToursRestants() {
        return nbToursRestants;
    }

    public void setNbToursRestants(int nbToursRestants) {
        this.nbToursRestants = nbToursRestants;
    }

    public int getIndexCurrentPlayer() {
        return indexCurrentPlayer;
    }

    public void setIndexCurrentPlayer(int indexCurrentPlayer) {
        this.indexCurrentPlayer = indexCurrentPlayer;
    }
    public boolean toNextPlayer(){//retourn true si on passe au tour suivant
        if(indexCurrentPlayer+1<lPlayers.size()){
            indexCurrentPlayer+=1;
            return false;
        }
        else{
            toNextTurn();
            return true;
        }
    }

    private void toNextTurn() {
        nbToursRestants-=1;
        //on change de premier joueur
        Player pCopy = lPlayers.remove(0);
        lPlayers.add(pCopy);
        //l'index du joueur qui joue passe à 1
        indexCurrentPlayer=0;
        //on fait recup les ressources aux joueurs
        recupRessources(map.getZonePierre(), Ressource.PIERRE);
        recupRessources(map.getCourRoi(), Ressource.METAL);
        recupRessources(map.getZoneSable(), Ressource.SABLE);
        recupRessources(map.getZoneBois(), Ressource.BOIS);
        for (Player p: lPlayers){
            p.getInventory().replace(Ressource.SMALLWORKER,p.getInventory().get(Ressource.SMALLWORKER),7);
        }
        refreshZonesBatisseurs();
    }
    private void recupRessources(PawsZone pz, Ressource ressource){
        for(PlayersColors c:PlayersColors.values()){
            int n = pz.getNbPawsPerColor().get(c);
            for(Player p: lPlayers){
                if(p.getColor()==c) {
                    int nbInit=p.getInventory().get(ressource);
                    p.getInventory().put(ressource, nbInit+n);
                }
            }
            pz.getNbPawsPerColor().put(c,0);
        }
    }
    public void exchangeAllRessources(){
        for(Player p:lPlayers){
            for (Ressource r: Ressource.values()){
                if(r.equals(Ressource.METAL) || r.equals(Ressource.PIERRE)
                || r.equals(Ressource.BOIS) || r.equals(Ressource.SABLE))
                p.addToNumberVictory(p.getInventory().get(r)*r.getValue());
            }
        }
    }
    public Player getCurrentPlayer(){
        return lPlayers.get(indexCurrentPlayer);
    }
    /*
    methode pour prelever obtenir la taxe du roi
     */
    public int getKingTax(){
        int taxeValue= (int) (Math.random()*6+1);
        switch (taxeValue){
            case 1: taxeValue=2;break;
            case 2:
            case 3:taxeValue = 3; break;
            case 4:
            case 5:taxeValue=4; break;
            default:taxeValue=5;break;
        }
        return taxeValue;
    }
    /*
    methode pour appliquer la taxe du roi
    @param lPlayer: la liste des joueurs du jeu
    @param LPlayerInKingCourt: liste des joueurs dont un batisseur est dans la cour du roi
     */
    public void applyKingTax(ArrayList<Player> lPlayers ,ArrayList<Player> lPlayerInKingCourt, int taxValue){
        for(Player p:lPlayers){
            if(!lPlayerInKingCourt.contains(p)){
                p.pay(taxValue, true);
            }
        }
    }
    public ArrayList<ZonesBatisseurs> getlZonesBatisseurs() {
        return lZonesBatisseurs;
    }

}
