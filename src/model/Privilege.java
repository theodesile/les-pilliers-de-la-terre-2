package model;

public enum Privilege {

    SALLY, //  ignorer les besoins des artisans
    CASTLEFREED, // (fait) recoit une metal ou 6 pieces d'or
    BISHOP, //  gagner 2 pièces d'or chaque fois notre master builder est pioché et on le place pas
    REGAN, //  on peut acheter 1 point avec 3 pièces d'or quand on veut
    COUNT, // (fait) recoit 2 pieces d'or au debut de chaque partie
    ALFRED, //choisir les 2 artisans posés en phase 1
    THOMAS, // (fait) immédiatement gagner 2 ressources de type choisi sauf metal
    EMPRESS, //  acheter et vendre du marché la quantité qu'on veut
    JONATHAN, //  on pay -1 pr chaque placement de master builder
    WILLIAM; // on recoit le master builder noir s'il est pioché on le met où on veut

}
