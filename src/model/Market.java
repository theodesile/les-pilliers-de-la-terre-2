package model;

import model.ressources.Ressource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
classe qui modéliser le marché de kingsbridge
 */
public class Market {
    private ArrayList<Player> lExchangers;
    private Map<Ressource, Integer> availableRessources;
    private int indexPlayer;

    public Market(){
        availableRessources=new HashMap<>();
        initMarket();
    }
    public void initMarket(){
        for(Ressource r: Ressource.values()){
            availableRessources.put(r, 4);
        }
        lExchangers= new ArrayList<>();
        indexPlayer=0;
    }
    public boolean buy(Ressource r, int number, Player p){
        int price=number*r.getValue();
        boolean pay;
        if(r.isExcheangeable() && r!=Ressource.METAL &&
                number<=availableRessources.get(r)) {
            pay = p.pay(price, false);
            if(!pay)
                return false;
            int initNumber=p.getInventory().get(r);
            p.getInventory().put(r, initNumber+number);
        }
        else{
            return false;
        }
        return true;
    }
    public boolean sell(Ressource r, int number, Player p){
        if(!r.isExcheangeable() || p.getInventory().get(r)<number)
            return false;
        int price=r.getValue()*number;
        int initValue=p.getInventory().get(Ressource.GOLD);
        p.getInventory().put(Ressource.GOLD, initValue+price);
        initValue=availableRessources.get(r);
        availableRessources.put(r,initValue+number);
        return true;
    }
    public ArrayList<Player> getlExchangers() {
        return lExchangers;
    }
    public void addExchanger(Player p){
        lExchangers.add(p);
    }
    public Player removeExchanger(Player p){
        if(lExchangers.remove(p))
            return p;
        return null;
    }

    public Map<Ressource, Integer> getAvailableRessources() {
        return availableRessources;
    }
    public Player getNextPlayer(){
        Player p=lExchangers.get(indexPlayer);
        indexPlayer+=1;
        if(indexPlayer>=lExchangers.size())
            indexPlayer=0;
        return p;
    }
}
