package model;

import model.ressources.Ressource;

import java.util.HashMap;
import java.util.Map;

public class CarteArtisan extends Carte {
    private Map<Ressource,Integer> ressources;
    private int gold;
    private int points;

    public CarteArtisan(HashMap<Ressource, Integer> ressources, int gold, int points) {
        this.ressources = ressources;
        this.gold = gold;
        this.points = points;

    }

    public void jouerCarte(Player player){
        player.setNumberVictoryPoints(player.getNumberVictoryPoints()+points);
        for(Map.Entry<Ressource, Integer> entry : ressources.entrySet()) {
            Ressource cle = entry.getKey();
            Integer valeur = entry.getValue();
            player.getInventory().replace(cle,player.getInventory().get(cle)-valeur);
        }

    }



    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


}
